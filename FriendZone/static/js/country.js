<script>
var con = [
{
name:'India',
capital:'New Delhi',
borders:["AFG","BGD","BTN","MMR","CHN","NPL","PAK","LKA"],
flag:'https://restcountries.eu/data/ind.svg',
},
{
name:'Afghanistan',
capital:'Kabul',
borders:["IRN","PAK","TKM","UZB","TJK","CHN"],
flag:'https://restcountries.eu/data/afg.svg',
},
{
name:'Pakistan',
capital:'Islamabad',
borders:["AFG","CHN","IND","IRN"],
flag:'https://restcountries.eu/data/pak.svg',
},
{
name:'China',
capital:'Beijing',
borders:["AFG","BTN","MMR","HKG","IND","KAZ","PRK","KGZ","LAO","MAC","MNG","PAK","RUS","TJK","VNM"],
flag:'https://restcountries.eu/data/chn.svg',
},
]
</script>